import babel from "rollup-plugin-babel";
import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";

export default [
  // browser-friendly UMD build
  {
    external: id => /^react|emotion|@emotion\/core|@emotion\/styled/.test(id),
    input: "./src/index.js",
    output: {
      file: "./index.js",
      format: "cjs"
    },
    plugins: [
      babel({
        exclude: "node_modules/**"
      }),
      resolve(),
      commonjs()
    ]
  },
];
