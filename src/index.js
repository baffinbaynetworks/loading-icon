import React from "react";
import styled from "@emotion/styled";
import { css } from "emotion";

const WrapperCss = ({
  colors = ["#1994FD", "#1C9EFE", "#20B3FB", "#23C6FB", "#23D3FB"],
  shadow = "0 0 10px #A4F9FC",
  size = 10,
  jump = 16,
  padding = 6,
  borderRadius = 2
}) => css`
  .wrapper {
    width: ${(size * 5 + padding * 4) / 2}px;
    height: ${size / 2}px;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
  }
  .block {
    position: absolute;
    width: ${size}px;
    height: ${size}px;
    border-radius: ${borderRadius}px;
    box-shadow: ${shadow};
  }
  .block:nth-of-type(1) {
    left: 0;
    background-color: ${colors[0]};
  }
  .block:nth-of-type(2) {
    left: ${(size + padding) * 1}px;
    background-color: ${colors[1]};
  }
  .block:nth-of-type(3) {
    left: ${(size + padding) * 2}px;
    background-color: ${colors[2]};
  }
  .block:nth-of-type(4) {
    left: ${(size + padding) * 3}px;
    background-color: ${colors[3]};
  }
  .block:nth-of-type(5) {
    left: ${(size + padding) * 4}px;
    background-color: ${colors[4]};
  }
  @keyframes slide {
    0%,
    8% {
      transform: translateX(0);
      background-color: ${colors[0]};
    }
    92%,
    100% {
      transform: translateX(${(padding + size) * 4}px);
      background-color: ${colors[4]};
    }
  }
  .mover {
    animation: slide 1.3s infinite alternate
      cubic-bezier(0.645, 0.045, 0.355, 1);
  }
  @keyframes jump1 {
    0% {
      top: 0;
      transform: rotate(0);
    }
    7.5% {
      top: -${jump}px;
      transform: rotateZ(-90deg);
    }
    15% {
      top: 0;
      height: ${size}px;
      transform: rotateZ(-180deg);
    }
    19% {
      top: ${size / 3}px;
      height: ${(size / 3) * 2}px;
      transform: rotateZ(-180deg);
    }
    23%,
    70% {
      top: 0;
      height: ${size}px;
      transform: rotateZ(-180deg);
    }
    77.5% {
      top: -${jump}px;
      transform: rotateZ(-90deg);
    }
    85% {
      top: 0;
      height: ${size}px;
      transform: rotateZ(0);
    }
    89% {
      top: ${size / 3}px;
      height: ${(size / 3) * 2}px;
      transform: rotateZ(0);
    }
    94%,
    100% {
      top: 0;
      height: ${size};
      transform: rotateZ(0);
    }
  }
  @keyframes jump2 {
    0% {
      top: 0;
      transform: rotate(0);
    }
    7.5% {
      top: -${jump}px;
      transform: rotateZ(-90deg);
    }
    15% {
      top: 0;
      height: ${size}px;
      transform: rotateZ(-180deg);
    }
    19% {
      top: ${size / 3}px;
      height: ${(size / 3) * 2}px;
      transform: rotateZ(-180deg);
    }
    23%,
    57.5% {
      top: 0;
      height: ${size}px;
      transform: rotateZ(-180deg);
    }
    65% {
      top: -${jump}px;
      transform: rotateZ(-90deg);
    }
    72.5% {
      top: 0;
      height: ${size}px;
      transform: rotateZ(0);
    }
    76.5% {
      top: ${size / 3}px;
      height: ${(size / 3) * 2}px;
      transform: rotateZ(0);
    }
    80.5%,
    100% {
      top: 0;
      height: ${size};
      transform: rotateZ(0);
    }
  }
  @keyframes jump3 {
    0% {
      top: 0;
      transform: rotate(0);
    }
    7.5% {
      top: -${jump}px;
      transform: rotateZ(-90deg);
    }
    15% {
      top: 0;
      height: ${size}px;
      transform: rotateZ(-180deg);
    }
    19% {
      top: ${size / 3}px;
      height: ${(size / 3) * 2}px;
      transform: rotateZ(-180deg);
    }
    23%,
    45% {
      top: 0;
      height: ${size}px;
      transform: rotateZ(-180deg);
    }
    52.5% {
      top: -${jump}px;
      transform: rotateZ(-90deg);
    }
    60% {
      top: 0;
      height: ${size}px;
      transform: rotateZ(0);
    }
    64% {
      top: ${size / 3}px;
      height: ${(size / 3) * 2}px;
      transform: rotateZ(0);
    }
    68%,
    100% {
      top: 0;
      height: ${size};
      transform: rotateZ(0);
    }
  }
  @keyframes jump4 {
    0% {
      top: 0;
      transform: rotate(0);
    }
    7.5% {
      top: -${jump}px;
      transform: rotateZ(-90deg);
    }
    15% {
      top: 0;
      height: ${size}px;
      transform: rotateZ(-180deg);
    }
    19% {
      top: ${size / 3}px;
      height: ${(size / 3) * 2}px;
      transform: rotateZ(-180deg);
    }
    23%,
    32.5% {
      top: 0;
      height: ${size}px;
      transform: rotateZ(-180deg);
    }
    40% {
      top: -${jump}px;
      transform: rotateZ(-90deg);
    }
    47.5% {
      top: 0;
      height: ${size}px;
      transform: rotateZ(0);
    }
    51.5% {
      top: ${size / 3}px;
      height: ${(size / 3) * 2}px;
      transform: rotateZ(0);
    }
    55.5%,
    100% {
      top: 0;
      height: ${size};
      transform: rotateZ(0);
    }
  }
  .jumper {
    transform-origin: -${padding / 2}px;
  }
  .jumper:nth-of-type(2) {
    animation: jump1 2.6s 0.2s infinite linear;
  }
  .jumper:nth-of-type(3) {
    animation: jump2 2.6s 0.35s infinite linear;
  }
  .jumper:nth-of-type(4) {
    animation: jump3 2.6s 0.5s infinite linear;
  }
  .jumper:nth-of-type(5) {
    animation: jump4 2.6s 0.65s infinite linear;
  }
`;

const Wrapper = styled("div")`
  ${WrapperCss}
`;

const LoadingIcon = ({ colors, shadow, size, jump, padding, borderRadius }) => {
  return (
    <Wrapper
      colors={colors}
      shadow={shadow}
      size={size}
      jump={jump}
      padding={padding}
      borderRadius={borderRadius}
    >
      <div className="wrapper">
        <div className="block mover" />
        <div className="block jumper" />
        <div className="block jumper" />
        <div className="block jumper" />
        <div className="block jumper" />
      </div>
    </Wrapper>
  );
};

export default LoadingIcon;

export const StaticIcon = () => {
  <div>
      <div className="wrapper">
        <div className="block mover" />
        <div className="block jumper" />
        <div className="block jumper" />
        <div className="block jumper" />
        <div className="block jumper" />
      </div>
  </div>;
};
